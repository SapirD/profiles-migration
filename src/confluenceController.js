const axiosStatic = require('axios').default;

const CONFLUENCE_HOST = 'url';
// const CONFLUENCE_HOST = 'url2';
const CONFLUENCE_USERNAME = 'username';
const CONFLUENCE_PASSWORD = 'password';
const AUTH_CONFIG = {
    auth: {
        username: CONFLUENCE_USERNAME, password: CONFLUENCE_PASSWORD
    },
    responseType: 'json',
    proxy: {
        host: 'host', port: 1000
    }
};
const PROFILE_PROP_KEY = "viewParametersProfile";

exports.getPagesBySpaceAndLabel = (space, labelName) => {
    const limitNum = 50;

    const uri = `/rest/api/content/search?cql=(type=page and space="${space}" and label=` +
        `"${labelName}")&limit=${limitNum}`;
    return new Promise((resolve, reject) => {
        getAllPagesWithPagination(uri, [], resolve, reject);
    });

};

exports.getPagesByFolder = (folderId) => {
    const uri = `/rest/api/content/${folderId}/child/page`;
    return new Promise((resolve, reject) => {
        getAllPagesWithPagination(uri, [], resolve, reject);
    });
};

exports.postPageProfile = async (profileName, id) => {
    const url = CONFLUENCE_HOST + `/rest/api/content/${id}/property`;
    const data = {
        key: PROFILE_PROP_KEY,
        value: profileName
    };
    let response;

    try {
        response = await axiosStatic.post(url, data, AUTH_CONFIG);
        return response;
    } catch (e) {
        if (e.response && e.response.status === 409) {
            await axiosStatic.delete(url + "/" + data.key, AUTH_CONFIG);
            response = await axiosStatic.post(url, data, AUTH_CONFIG);
            return response;
        } else {
            throw e;
        }
    }
};

exports.deletePageProfile = (id) => {
    const url = CONFLUENCE_HOST + `/rest/api/content/${id}/property`;
    const data = {
        key: PROFILE_PROP_KEY
    };
    return axiosStatic.delete(url + "/" + data.key, AUTH_CONFIG);
};

exports.getPropertyByPageId = async (id) => {
    const uri = `/rest/api/content/${id}/property/${PROFILE_PROP_KEY}`;
    return await axiosStatic.get(CONFLUENCE_HOST + uri, AUTH_CONFIG)
        .then(res => {
            return res.data.value;
        })
        .catch(e => null);
};

const getAllPagesWithPagination = (uri, pages, resolve, reject) => {
    axiosStatic.get(CONFLUENCE_HOST + uri, AUTH_CONFIG).then(res => {
        let pagesData = res.data.results.map(objJson => {
            return ({
                id: objJson.id,
                title: objJson.title
            });
        });
        const retrievePages = pages.concat(pagesData);
        if (!!res.data._links.next) {
            getAllPagesWithPagination(res.data._links.next, retrievePages, resolve, reject);
        } else {
            resolve(retrievePages);
        }
    }).catch(e => {
        reject(e);
    });
};


