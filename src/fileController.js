const fs = require('fs');

exports.writeJsonToFile = (filePath, objJson) => {
    fs.writeFileSync(filePath, objJson, 'utf-8');
};

exports.appendJsonToFile = (filePath, objJson) => {
    const jsonToAppend = objJson + "\n";
    fs.appendFile(filePath, jsonToAppend,'utf-8',()=>{});
};

exports.readJsonFromFile = (filePath) => {
    return JSON.parse(fs.readFileSync(filePath, 'utf-8'));
};

exports.readJsonsArrFromFile = (filePath) => {
    const arrJsons =  fs.readFileSync(filePath, 'utf-8').split("\n");
    return arrJsons;
};
