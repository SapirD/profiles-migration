const {getPagesBySpaceAndLabel,  postPageProfile, getPropertyByPageId, deletePageProfile,
    getPagesByFolder} = require('./confluenceController.js');
const {writeJsonToFile, readJsonFromFile, appendJsonToFile, readJsonsArrFromFile} = require('./fileController.js');

const labels = {
    EPIC: 'objecttype-epicdeliverable',
    DELIVERABLE: 'objecttype-deliverable',
    BP: 'objecttype-bp'
};
const spaceKey = {
    BEELINE: 'BEE001',
    VFDE: 'VDE002',
    XL_AXIATA: 'XLA003',
    ORANGE_SPAIN: 'ORGS001',
    CHARTER: 'CHAR001',
    VFRO: 'BP26',
    VFIT: 'VFIT01',
    GLOBE: 'GLOBE001',
    T_MOBILE: 'TMOBILE004'
};
let startTime = 0;

const run = async () => {
    // Log time
     startTime = Date.now();

    // await backupPagesProfiles(spaceKey.BEELINE, labels.EPIC, "gr").then(_=> console.log("success epic"));
    // await backupPagesProfilesByFolder(102420791, spaceKey.BEELINE, labels.DELIVERABLE, "gr").then(_=> console.log("success deliv"));

    // Create file with profiles - TODO: change before execution
    const jsonToFile = await migrateSpaceData(spaceKey.VFRO,
        "VFRO EPIC Deliverables", "VFRO BP Deliverables");
    // const jsonToFile = await migrateSpaceDataDelWithFolder(spaceKey.BEELINE,
    //     "Beeline EPIC Deliverable", "Beeline BP Deliverable", 102420791);
    writeJsonToFile(`./data/pagesDataToMigrate/prod/${spaceKey.VFRO}-pagesWithProfiles`,
        JSON.stringify(jsonToFile));

    // TODO: change before execution
    // applyMigrationSpace(spaceKey.BEELINE, "gr").catch(e => {throw e});

    // restoreBackup("AN01", `./data/backup/gr/AN01`);
};

const applyMigrationSpace = async (space, workspace) => {
    // Get data for post from file
    let dataFromFile = readJsonFromFile(`./data/pagesDataToMigrate/${workspace}/${space}-pagesWithProfiles`);
    let size = dataFromFile.pages.length;
    // console.log(dataFromFile.pages[size-1]);

    // Post profiles
    const postPromises = [];
    dataFromFile.pages.forEach(objPage => {
        // console.log(objPage.id + ": " + objPage.profile);
        let postPromise = postPageProfile(objPage.profile, objPage.id)
            .then(postResponse => appendJsonToFile(`./data/migrationResults/${workspace}/${space}-results`,
                JSON.stringify(postResponse.data)))
            .catch(e => {
                console.error("Error - refer to log");
                appendJsonToFile(`./data/logs/${workspace}/${space}-log`, JSON.stringify(e));
                throw e;
            });
        postPromises.push(postPromise);
    });

    Promise.all(postPromises).then(res => {
        const total = (Date.now() - startTime)/1000;
        console.log("Time:" + total);
        writeJsonToFile(`./data/logs/${workspace}/${space}-log`, "Time: " + total.toString() + '\n');
        appendJsonToFile(`./data/logs/${workspace}/${space}-log`, "Size of source file: " + size.toString());
        const arrResults = readJsonsArrFromFile(`./data/migrationResults/${workspace}/${space}-results`);
        appendJsonToFile(`./data/logs/${workspace}/${space}-log`, "Size of result file: " + arrResults.length);
    }).catch(e => {
        throw e;
    });
}

const restoreBackup = (space, filePath) => {
    const arrBackupData = readJsonsArrFromFile(filePath).filter(pageString => pageString !== "");
    // console.log(arrBackupData);
    arrBackupData.forEach(page => {
        let currentPage = JSON.parse(page);
        if (!!currentPage && (!currentPage.profile || currentPage.profile === "null")) {
            getPropertyByPageId(currentPage.id).then(res => {
                if(!!res) {
                    deletePageProfile(currentPage.id)
                        .catch(e => appendJsonToFile(`./data/backup/${space}-log`, JSON.stringify(e)));
                }
            });
        } else if (!!currentPage) {
            postPageProfile(currentPage.profile, currentPage.id).then(postResponse => appendJsonToFile(`./data/backup/${space}-results`,
                JSON.stringify(postResponse.data)))
                .catch(e => {
                    console.error("Error - refer to log");
                    appendJsonToFile(`./data/backup/${space}-log`, JSON.stringify(e));
                    throw e;
                });
        }
    });
}

const migrateSpaceData = async (spaceKey, epicProfile, delProfile) => {
    let pagesWithProfile =
        await createPagesProfilesArray(spaceKey,labels.EPIC, epicProfile);
    pagesWithProfile = pagesWithProfile.concat(
        await createPagesProfilesArray(spaceKey,labels.DELIVERABLE, delProfile));
    return {
        pages: pagesWithProfile
    }
}

const migrateSpaceDataDelWithFolder = async (spaceKey, epicProfile, delProfile, delFolder) => {
    let pagesWithProfile =
        await createPagesProfilesArray(spaceKey,labels.EPIC, epicProfile);
    pagesWithProfile = pagesWithProfile.concat(
        await createPagesProfilesArrayFromFolder(delProfile, delFolder));
    return {
        pages: pagesWithProfile
    }
}

const createPagesProfilesArray = async (space, label, profile) => {
    const arrPagesData = await getPagesBySpaceAndLabel(space, label);

    const pagesWithProfile = arrPagesData.map(page => {
        return ({
            ...page,
            profile: profile
        });
    });

    return pagesWithProfile;
}

const createPagesProfilesArrayFromFolder = async (profile, folder) => {
    const arrPagesData = await getPagesByFolder(folder);

    const pagesWithProfile = arrPagesData.map(page => {
        return ({
            ...page,
            profile: profile
        });
    });

    return pagesWithProfile;
}

const backupPagesProfiles = async (space, label, workspace) => {
    const arrPagesData = await getPagesBySpaceAndLabel(space, label);
    try {

        arrPagesData.forEach(page => {
            // get selected profile
            let pageProfileData = getPropertyByPageId(page.id);
            pageProfileData.then(profileValue => {
                // if there is a selected profile - write this page with profile in a file
                appendJsonToFile(`./data/backup/${workspace}/${space}`,
                    JSON.stringify({...page, profile: profileValue, pageType: label}));
            });
        });
    } catch (e) {
        throw e;
    }

}

const backupPagesProfilesByFolder = async (folder, space, label, workspace) => {
    const arrPagesData = await getPagesByFolder(folder);
    try {

        arrPagesData.forEach(page => {
            // get selected profile
            let pageProfileData = getPropertyByPageId(page.id);
            pageProfileData.then(profileValue => {
                // if there is a selected profile - write this page with profile in a file
                appendJsonToFile(`./data/backup/${workspace}/${space}`,
                    JSON.stringify({...page, profile: profileValue, pageType: label}));
            });
        });
    } catch (e) {
        throw e;
    }

}

run()
    .then(res => {
    })
    .catch(e => console.error(e));